export interface Period {
  id: number;
  purpose: any;
  dateFrom: string;
  dateTo: string;
  employee: number;
}
