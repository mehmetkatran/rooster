package com.example.rooster.period;

/**
 * Enum Class for possible reasons of absence or presence
 */
public enum Purpose {
    VACATION_REQUEST,
    CONFIRMED_VACATION,
    SICK_LEAVE,
    WORKING_HOURS,
    ABSENCE,
    FREE_TIME_REQUEST,
    WORKING_HOUR_REQUEST
}
